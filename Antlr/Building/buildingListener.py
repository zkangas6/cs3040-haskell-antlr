# Generated from building.g4 by ANTLR 4.9.2
from antlr4 import *
if __name__ is not None and "." in __name__:
    from .buildingParser import buildingParser
else:
    from buildingParser import buildingParser

class Floor:
    class Room:
        def __init__(self, x, y):
            self.x = x
            self.y = y

        def area(self):
            return self.x*self.y

    def __init__(self, name):
        self.name = name
        self.rooms = []

    def add_room(self, x, y):
        self.rooms.append(Floor.Room(x,y))

    def area(self):
        x = 0
        for room in self.rooms:
            x += room.area()
        return x


class Building:
    def __init__(self, name):
        self.name = name
        self.floor_list = []

    def add_floor(self, floor):
        if (floor is not None):
            self.floor_list.append(floor)

    def area(self):
        x = 0
        for floor in self.floor_list:
            x += floor.area()
        return x

def print_buildings(list_floors=False):
    total_area = 0
    for building in building_list:
        area = building.area()
        total_area += area
        print("Area for building {name}: {area} square feet.".format(name=building.name, area=area))
        if list_floors:
            print("and has floors:")
            for floor in building.floor_list:
                print("\tfloor", floor.name, "has area", floor.area())
    print("The total usable area is {size} square feet.".format(size=total_area))

floor_list = []
building_list = []


# This class defines a complete listener for a parse tree produced by buildingParser.
class buildingListener(ParseTreeListener):

    # Enter a parse tree produced by buildingParser#plans.
    def enterPlans(self, ctx:buildingParser.PlansContext):
        pass

    # Exit a parse tree produced by buildingParser#plans.
    def exitPlans(self, ctx:buildingParser.PlansContext):
        pass


    # Enter a parse tree produced by buildingParser#complexB.
    def enterComplexB(self, ctx:buildingParser.ComplexBContext):
        pass

    # Exit a parse tree produced by buildingParser#complexB.
    def exitComplexB(self, ctx:buildingParser.ComplexBContext):
        pass


    # Enter a parse tree produced by buildingParser#building.
    def enterBuilding(self, ctx:buildingParser.BuildingContext):
        pass

    # Exit a parse tree produced by buildingParser#building.
    def exitBuilding(self, ctx:buildingParser.BuildingContext):
        pass


    # Enter a parse tree produced by buildingParser#floorList.
    def enterFloorList(self, ctx:buildingParser.FloorListContext):
        pass

    # Exit a parse tree produced by buildingParser#floorList.
    def exitFloorList(self, ctx:buildingParser.FloorListContext):
        pass


    # Enter a parse tree produced by buildingParser#floorReference.
    def enterFloorReference(self, ctx:buildingParser.FloorReferenceContext):
        pass

    # Exit a parse tree produced by buildingParser#floorReference.
    def exitFloorReference(self, ctx:buildingParser.FloorReferenceContext):
        pass


    # Enter a parse tree produced by buildingParser#floor.
    def enterFloor(self, ctx:buildingParser.FloorContext):
        pass

    # Exit a parse tree produced by buildingParser#floor.
    def exitFloor(self, ctx:buildingParser.FloorContext):
        pass


    # Enter a parse tree produced by buildingParser#roomList.
    def enterRoomList(self, ctx:buildingParser.RoomListContext):
        pass

    # Exit a parse tree produced by buildingParser#roomList.
    def exitRoomList(self, ctx:buildingParser.RoomListContext):
        pass


    # Enter a parse tree produced by buildingParser#room.
    def enterRoom(self, ctx:buildingParser.RoomContext):
        pass

    # Exit a parse tree produced by buildingParser#room.
    def exitRoom(self, ctx:buildingParser.RoomContext):
        pass


    # Enter a parse tree produced by buildingParser#name.
    def enterName(self, ctx:buildingParser.NameContext):
        pass

    # Exit a parse tree produced by buildingParser#name.
    def exitName(self, ctx:buildingParser.NameContext):
        pass


    # Enter a parse tree produced by buildingParser#number.
    def enterNumber(self, ctx:buildingParser.NumberContext):
        pass

    # Exit a parse tree produced by buildingParser#number.
    def exitNumber(self, ctx:buildingParser.NumberContext):
        pass


    # Enter a parse tree produced by buildingParser#floorName.
    def enterFloorName(self, ctx:buildingParser.FloorNameContext):
        pass

    # Exit a parse tree produced by buildingParser#floorName.
    def exitFloorName(self, ctx:buildingParser.FloorNameContext):
        pass


    # Enter a parse tree produced by buildingParser#empty.
    def enterEmpty(self, ctx:buildingParser.EmptyContext):
        pass

    # Exit a parse tree produced by buildingParser#empty.
    def exitEmpty(self, ctx:buildingParser.EmptyContext):
        pass



del buildingParser