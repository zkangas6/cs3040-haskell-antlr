# Generated from building.g4 by ANTLR 4.9.2
# encoding: utf-8
from antlr4 import *
from io import StringIO
import sys
if sys.version_info[1] > 5:
	from typing import TextIO
else:
	from typing.io import TextIO


class Floor:
    class Room:
        def __init__(self, x, y):
            self.x = x
            self.y = y

        def area(self):
            return self.x*self.y

    def __init__(self, name):
        self.name = name
        self.rooms = []

    def add_room(self, x, y):
        self.rooms.append(Floor.Room(x,y))

    def area(self):
        x = 0
        for room in self.rooms:
            x += room.area()
        return x


class Building:
    def __init__(self, name):
        self.name = name
        self.floor_list = []

    def add_floor(self, floor):
        if (floor is not None):
            self.floor_list.append(floor)

    def area(self):
        x = 0
        for floor in self.floor_list:
            x += floor.area()
        return x

def print_buildings(list_floors=False):
    total_area = 0
    for building in building_list:
        area = building.area()
        total_area += area
        print("Area for building {name}: {area} square feet.".format(name=building.name, area=area))
        if list_floors:
            print("and has floors:")
            for floor in building.floor_list:
                print("\tfloor", floor.name, "has area", floor.area())
    print("The total usable area is {size} square feet.".format(size=total_area))

floor_list = []
building_list = []


def serializedATN():
    with StringIO() as buf:
        buf.write("\3\u608b\ua72a\u8133\ub9ed\u417c\u3be7\u7786\u5964\3\21")
        buf.write("h\4\2\t\2\4\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7\4\b")
        buf.write("\t\b\4\t\t\t\4\n\t\n\4\13\t\13\4\f\t\f\4\r\t\r\3\2\6\2")
        buf.write("\34\n\2\r\2\16\2\35\3\2\3\2\3\2\3\2\3\3\3\3\6\3&\n\3\r")
        buf.write("\3\16\3\'\3\4\3\4\3\4\3\4\3\4\3\4\3\4\3\4\3\4\3\5\3\5")
        buf.write("\3\5\3\5\3\5\3\5\3\5\3\5\5\5;\n\5\3\6\3\6\3\6\3\7\3\7")
        buf.write("\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\b\3\b\3\b\3\b\3\b\3\b\3")
        buf.write("\b\3\b\5\bQ\n\b\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t\5\t[\n")
        buf.write("\t\3\n\3\n\3\n\3\13\3\13\3\13\3\f\3\f\3\f\3\r\3\r\3\r")
        buf.write("\2\2\16\2\4\6\b\n\f\16\20\22\24\26\30\2\2\2`\2\33\3\2")
        buf.write("\2\2\4#\3\2\2\2\6)\3\2\2\2\b:\3\2\2\2\n<\3\2\2\2\f?\3")
        buf.write("\2\2\2\16P\3\2\2\2\20Z\3\2\2\2\22\\\3\2\2\2\24_\3\2\2")
        buf.write("\2\26b\3\2\2\2\30e\3\2\2\2\32\34\5\f\7\2\33\32\3\2\2\2")
        buf.write("\34\35\3\2\2\2\35\33\3\2\2\2\35\36\3\2\2\2\36\37\3\2\2")
        buf.write("\2\37 \5\4\3\2 !\7\2\2\3!\"\b\2\1\2\"\3\3\2\2\2#%\7\t")
        buf.write("\2\2$&\5\6\4\2%$\3\2\2\2&\'\3\2\2\2\'%\3\2\2\2\'(\3\2")
        buf.write("\2\2(\5\3\2\2\2)*\7\b\2\2*+\5\22\n\2+,\7\n\2\2,-\7\13")
        buf.write("\2\2-.\7\3\2\2./\5\b\5\2/\60\7\4\2\2\60\61\b\4\1\2\61")
        buf.write("\7\3\2\2\2\62\63\5\n\6\2\63\64\b\5\1\2\64;\3\2\2\2\65")
        buf.write("\66\5\n\6\2\66\67\7\5\2\2\678\5\b\5\289\b\5\1\29;\3\2")
        buf.write("\2\2:\62\3\2\2\2:\65\3\2\2\2;\t\3\2\2\2<=\5\26\f\2=>\b")
        buf.write("\6\1\2>\13\3\2\2\2?@\7\13\2\2@A\5\22\n\2AB\7\f\2\2BC\7")
        buf.write("\r\2\2CD\7\6\2\2DE\5\16\b\2EF\7\7\2\2FG\b\7\1\2G\r\3\2")
        buf.write("\2\2HI\5\20\t\2IJ\b\b\1\2JQ\3\2\2\2KL\5\20\t\2LM\7\5\2")
        buf.write("\2MN\5\16\b\2NO\b\b\1\2OQ\3\2\2\2PH\3\2\2\2PK\3\2\2\2")
        buf.write("Q\17\3\2\2\2RS\5\24\13\2ST\5\20\t\2TU\b\t\1\2U[\3\2\2")
        buf.write("\2VW\7\16\2\2WX\5\24\13\2XY\b\t\1\2Y[\3\2\2\2ZR\3\2\2")
        buf.write("\2ZV\3\2\2\2[\21\3\2\2\2\\]\7\17\2\2]^\b\n\1\2^\23\3\2")
        buf.write("\2\2_`\7\20\2\2`a\b\13\1\2a\25\3\2\2\2bc\5\22\n\2cd\b")
        buf.write("\f\1\2d\27\3\2\2\2ef\3\2\2\2f\31\3\2\2\2\7\35\':PZ")
        return buf.getvalue()


class buildingParser ( Parser ):

    grammarFileName = "building.g4"

    atn = ATNDeserializer().deserialize(serializedATN())

    decisionsToDFA = [ DFA(ds, i) for i, ds in enumerate(atn.decisionToState) ]

    sharedContextCache = PredictionContextCache()

    literalNames = [ "<INVALID>", "'{'", "'}'", "','", "'['", "']'" ]

    symbolicNames = [ "<INVALID>", "<INVALID>", "<INVALID>", "<INVALID>", 
                      "<INVALID>", "<INVALID>", "BUILDING", "COMPLEXB", 
                      "WITH", "FLOOR", "HAS", "ROOMS", "BY", "NAME", "NUMBER", 
                      "WS" ]

    RULE_plans = 0
    RULE_complexB = 1
    RULE_building = 2
    RULE_floorList = 3
    RULE_floorReference = 4
    RULE_floor = 5
    RULE_roomList = 6
    RULE_room = 7
    RULE_name = 8
    RULE_number = 9
    RULE_floorName = 10
    RULE_empty = 11

    ruleNames =  [ "plans", "complexB", "building", "floorList", "floorReference", 
                   "floor", "roomList", "room", "name", "number", "floorName", 
                   "empty" ]

    EOF = Token.EOF
    T__0=1
    T__1=2
    T__2=3
    T__3=4
    T__4=5
    BUILDING=6
    COMPLEXB=7
    WITH=8
    FLOOR=9
    HAS=10
    ROOMS=11
    BY=12
    NAME=13
    NUMBER=14
    WS=15

    def __init__(self, input:TokenStream, output:TextIO = sys.stdout):
        super().__init__(input, output)
        self.checkVersion("4.9.2")
        self._interp = ParserATNSimulator(self, self.atn, self.decisionsToDFA, self.sharedContextCache)
        self._predicates = None




    class PlansContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def complexB(self):
            return self.getTypedRuleContext(buildingParser.ComplexBContext,0)


        def EOF(self):
            return self.getToken(buildingParser.EOF, 0)

        def floor(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(buildingParser.FloorContext)
            else:
                return self.getTypedRuleContext(buildingParser.FloorContext,i)


        def getRuleIndex(self):
            return buildingParser.RULE_plans

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterPlans" ):
                listener.enterPlans(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitPlans" ):
                listener.exitPlans(self)




    def plans(self):

        localctx = buildingParser.PlansContext(self, self._ctx, self.state)
        self.enterRule(localctx, 0, self.RULE_plans)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 25 
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while True:
                self.state = 24
                self.floor()
                self.state = 27 
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                if not (_la==buildingParser.FLOOR):
                    break

            self.state = 29
            self.complexB()
            self.state = 30
            self.match(buildingParser.EOF)
            print_buildings(list_floors=False)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class ComplexBContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def COMPLEXB(self):
            return self.getToken(buildingParser.COMPLEXB, 0)

        def building(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(buildingParser.BuildingContext)
            else:
                return self.getTypedRuleContext(buildingParser.BuildingContext,i)


        def getRuleIndex(self):
            return buildingParser.RULE_complexB

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterComplexB" ):
                listener.enterComplexB(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitComplexB" ):
                listener.exitComplexB(self)




    def complexB(self):

        localctx = buildingParser.ComplexBContext(self, self._ctx, self.state)
        self.enterRule(localctx, 2, self.RULE_complexB)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 33
            self.match(buildingParser.COMPLEXB)
            self.state = 35 
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while True:
                self.state = 34
                self.building()
                self.state = 37 
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                if not (_la==buildingParser.BUILDING):
                    break

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class BuildingContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser
            self._name = None # NameContext
            self._floorList = None # FloorListContext

        def BUILDING(self):
            return self.getToken(buildingParser.BUILDING, 0)

        def name(self):
            return self.getTypedRuleContext(buildingParser.NameContext,0)


        def WITH(self):
            return self.getToken(buildingParser.WITH, 0)

        def FLOOR(self):
            return self.getToken(buildingParser.FLOOR, 0)

        def floorList(self):
            return self.getTypedRuleContext(buildingParser.FloorListContext,0)


        def getRuleIndex(self):
            return buildingParser.RULE_building

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterBuilding" ):
                listener.enterBuilding(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitBuilding" ):
                listener.exitBuilding(self)




    def building(self):

        localctx = buildingParser.BuildingContext(self, self._ctx, self.state)
        self.enterRule(localctx, 4, self.RULE_building)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 39
            self.match(buildingParser.BUILDING)
            self.state = 40
            localctx._name = self.name()
            self.state = 41
            self.match(buildingParser.WITH)
            self.state = 42
            self.match(buildingParser.FLOOR)
            self.state = 43
            self.match(buildingParser.T__0)
            self.state = 44
            localctx._floorList = self.floorList()
            self.state = 45
            self.match(buildingParser.T__1)

            namae= localctx._name.name_val
            roomies = localctx._floorList.floor_arr
            tfloor = Building(namae)
            for i in roomies:
                tfloor.add_floor(i)
            building_list.append(tfloor)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class FloorListContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser
            self.floor_arr = None
            self._floorReference = None # FloorReferenceContext
            self._floorList = None # FloorListContext

        def floorReference(self):
            return self.getTypedRuleContext(buildingParser.FloorReferenceContext,0)


        def floorList(self):
            return self.getTypedRuleContext(buildingParser.FloorListContext,0)


        def getRuleIndex(self):
            return buildingParser.RULE_floorList

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterFloorList" ):
                listener.enterFloorList(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitFloorList" ):
                listener.exitFloorList(self)




    def floorList(self):

        localctx = buildingParser.FloorListContext(self, self._ctx, self.state)
        self.enterRule(localctx, 6, self.RULE_floorList)
        try:
            self.state = 56
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,2,self._ctx)
            if la_ == 1:
                self.enterOuterAlt(localctx, 1)
                self.state = 48
                localctx._floorReference = self.floorReference()
                localctx.floor_arr = [localctx._floorReference.floor_x]
                pass

            elif la_ == 2:
                self.enterOuterAlt(localctx, 2)
                self.state = 51
                localctx._floorReference = self.floorReference()
                self.state = 52
                self.match(buildingParser.T__2)
                self.state = 53
                localctx._floorList = self.floorList()

                localctx.floor_arr = localctx._floorList.floor_arr
                localctx.floor_arr.append(localctx._floorReference.floor_x)
                pass


        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class FloorReferenceContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser
            self.floor_x = None
            self._floorName = None # FloorNameContext

        def floorName(self):
            return self.getTypedRuleContext(buildingParser.FloorNameContext,0)


        def getRuleIndex(self):
            return buildingParser.RULE_floorReference

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterFloorReference" ):
                listener.enterFloorReference(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitFloorReference" ):
                listener.exitFloorReference(self)




    def floorReference(self):

        localctx = buildingParser.FloorReferenceContext(self, self._ctx, self.state)
        self.enterRule(localctx, 8, self.RULE_floorReference)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 58
            localctx._floorName = self.floorName()
            namee = localctx._floorName.name_val
            localctx.floor_x = None
            for fl in floor_list:
                if fl.name == namee:
                    localctx.floor_x = fl
            fltemp = localctx.floor_x
            if fltemp is None:
                raise RecognitionException("Invalid reference to floor: " + namee + "")

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class FloorContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser
            self.floor_a = None
            self._name = None # NameContext
            self._roomList = None # RoomListContext

        def FLOOR(self):
            return self.getToken(buildingParser.FLOOR, 0)

        def name(self):
            return self.getTypedRuleContext(buildingParser.NameContext,0)


        def HAS(self):
            return self.getToken(buildingParser.HAS, 0)

        def ROOMS(self):
            return self.getToken(buildingParser.ROOMS, 0)

        def roomList(self):
            return self.getTypedRuleContext(buildingParser.RoomListContext,0)


        def getRuleIndex(self):
            return buildingParser.RULE_floor

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterFloor" ):
                listener.enterFloor(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitFloor" ):
                listener.exitFloor(self)




    def floor(self):

        localctx = buildingParser.FloorContext(self, self._ctx, self.state)
        self.enterRule(localctx, 10, self.RULE_floor)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 61
            self.match(buildingParser.FLOOR)
            self.state = 62
            localctx._name = self.name()
            self.state = 63
            self.match(buildingParser.HAS)
            self.state = 64
            self.match(buildingParser.ROOMS)
            self.state = 65
            self.match(buildingParser.T__3)
            self.state = 66
            localctx._roomList = self.roomList()
            self.state = 67
            self.match(buildingParser.T__4)
            namae= localctx._name.name_val
            for fl in floor_list:
                if fl.name == namae:
                    raise RecognitionException("Duplicate Floor Name: " + namae)
            roomies = localctx._roomList.room_arr
            tfloor = Floor(namae)
            for i in roomies:
                tfloor.add_room(i[0], i[1])
            floor_list.append(tfloor)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class RoomListContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser
            self.room_arr = None
            self._room = None # RoomContext
            self._roomList = None # RoomListContext

        def room(self):
            return self.getTypedRuleContext(buildingParser.RoomContext,0)


        def roomList(self):
            return self.getTypedRuleContext(buildingParser.RoomListContext,0)


        def getRuleIndex(self):
            return buildingParser.RULE_roomList

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterRoomList" ):
                listener.enterRoomList(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitRoomList" ):
                listener.exitRoomList(self)




    def roomList(self):

        localctx = buildingParser.RoomListContext(self, self._ctx, self.state)
        self.enterRule(localctx, 12, self.RULE_roomList)
        try:
            self.state = 78
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,3,self._ctx)
            if la_ == 1:
                self.enterOuterAlt(localctx, 1)
                self.state = 70
                localctx._room = self.room()
                localctx.room_arr = [localctx._room.room_xy]
                pass

            elif la_ == 2:
                self.enterOuterAlt(localctx, 2)
                self.state = 73
                localctx._room = self.room()
                self.state = 74
                self.match(buildingParser.T__2)
                self.state = 75
                localctx._roomList = self.roomList()

                localctx.room_arr = localctx._roomList.room_arr
                localctx.room_arr.append(localctx._room.room_xy)

                pass


        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class RoomContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser
            self.room_xy = None
            self._number = None # NumberContext
            self._room = None # RoomContext

        def number(self):
            return self.getTypedRuleContext(buildingParser.NumberContext,0)


        def room(self):
            return self.getTypedRuleContext(buildingParser.RoomContext,0)


        def BY(self):
            return self.getToken(buildingParser.BY, 0)

        def getRuleIndex(self):
            return buildingParser.RULE_room

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterRoom" ):
                listener.enterRoom(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitRoom" ):
                listener.exitRoom(self)




    def room(self):

        localctx = buildingParser.RoomContext(self, self._ctx, self.state)
        self.enterRule(localctx, 14, self.RULE_room)
        try:
            self.state = 88
            self._errHandler.sync(self)
            token = self._input.LA(1)
            if token in [buildingParser.NUMBER]:
                self.enterOuterAlt(localctx, 1)
                self.state = 80
                localctx._number = self.number()
                self.state = 81
                localctx._room = self.room()
                temp = localctx._room.room_xy
                temp.append(localctx._number.value)
                localctx.room_xy=temp
                pass
            elif token in [buildingParser.BY]:
                self.enterOuterAlt(localctx, 2)
                self.state = 84
                self.match(buildingParser.BY)
                self.state = 85
                localctx._number = self.number()
                localctx.room_xy = [localctx._number.value]
                pass
            else:
                raise NoViableAltException(self)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class NameContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser
            self.name_val = None
            self._NAME = None # Token

        def NAME(self):
            return self.getToken(buildingParser.NAME, 0)

        def getRuleIndex(self):
            return buildingParser.RULE_name

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterName" ):
                listener.enterName(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitName" ):
                listener.exitName(self)




    def name(self):

        localctx = buildingParser.NameContext(self, self._ctx, self.state)
        self.enterRule(localctx, 16, self.RULE_name)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 90
            localctx._NAME = self.match(buildingParser.NAME)
            localctx.name_val = str((None if localctx._NAME is None else localctx._NAME.text))
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class NumberContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser
            self.value = None
            self._NUMBER = None # Token

        def NUMBER(self):
            return self.getToken(buildingParser.NUMBER, 0)

        def getRuleIndex(self):
            return buildingParser.RULE_number

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterNumber" ):
                listener.enterNumber(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitNumber" ):
                listener.exitNumber(self)




    def number(self):

        localctx = buildingParser.NumberContext(self, self._ctx, self.state)
        self.enterRule(localctx, 18, self.RULE_number)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 93
            localctx._NUMBER = self.match(buildingParser.NUMBER)
            localctx.value = int((None if localctx._NUMBER is None else localctx._NUMBER.text))
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class FloorNameContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser
            self.name_val = None
            self._name = None # NameContext

        def name(self):
            return self.getTypedRuleContext(buildingParser.NameContext,0)


        def getRuleIndex(self):
            return buildingParser.RULE_floorName

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterFloorName" ):
                listener.enterFloorName(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitFloorName" ):
                listener.exitFloorName(self)




    def floorName(self):

        localctx = buildingParser.FloorNameContext(self, self._ctx, self.state)
        self.enterRule(localctx, 20, self.RULE_floorName)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 96
            localctx._name = self.name()
            localctx.name_val = str(localctx._name.name_val)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class EmptyContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser


        def getRuleIndex(self):
            return buildingParser.RULE_empty

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterEmpty" ):
                listener.enterEmpty(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitEmpty" ):
                listener.exitEmpty(self)




    def empty(self):

        localctx = buildingParser.EmptyContext(self, self._ctx, self.state)
        self.enterRule(localctx, 22, self.RULE_empty)
        try:
            self.enterOuterAlt(localctx, 1)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx





