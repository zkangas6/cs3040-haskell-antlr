# Generated from building.g4 by ANTLR 4.9.2
from antlr4 import *
from io import StringIO
import sys
if sys.version_info[1] > 5:
    from typing import TextIO
else:
    from typing.io import TextIO


class Floor:
    class Room:
        def __init__(self, x, y):
            self.x = x
            self.y = y

        def area(self):
            return self.x*self.y

    def __init__(self, name):
        self.name = name
        self.rooms = []

    def add_room(self, x, y):
        self.rooms.append(Floor.Room(x,y))

    def area(self):
        x = 0
        for room in self.rooms:
            x += room.area()
        return x


class Building:
    def __init__(self, name):
        self.name = name
        self.floor_list = []

    def add_floor(self, floor):
        if (floor is not None):
            self.floor_list.append(floor)

    def area(self):
        x = 0
        for floor in self.floor_list:
            x += floor.area()
        return x

def print_buildings(list_floors=False):
    total_area = 0
    for building in building_list:
        area = building.area()
        total_area += area
        print("Area for building {name}: {area} square feet.".format(name=building.name, area=area))
        if list_floors:
            print("and has floors:")
            for floor in building.floor_list:
                print("\tfloor", floor.name, "has area", floor.area())
    print("The total usable area is {size} square feet.".format(size=total_area))

floor_list = []
building_list = []



def serializedATN():
    with StringIO() as buf:
        buf.write("\3\u608b\ua72a\u8133\ub9ed\u417c\u3be7\u7786\u5964\2\21")
        buf.write("h\b\1\4\2\t\2\4\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7")
        buf.write("\4\b\t\b\4\t\t\t\4\n\t\n\4\13\t\13\4\f\t\f\4\r\t\r\4\16")
        buf.write("\t\16\4\17\t\17\4\20\t\20\3\2\3\2\3\3\3\3\3\4\3\4\3\5")
        buf.write("\3\5\3\6\3\6\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\b\3")
        buf.write("\b\3\b\3\b\3\b\3\b\3\b\3\b\3\t\3\t\3\t\3\t\3\t\3\n\3\n")
        buf.write("\3\n\3\n\3\n\3\n\5\nH\n\n\3\13\3\13\3\13\3\13\3\f\3\f")
        buf.write("\3\f\3\f\3\f\5\fS\n\f\3\r\3\r\3\r\3\16\6\16Y\n\16\r\16")
        buf.write("\16\16Z\3\17\6\17^\n\17\r\17\16\17_\3\20\6\20c\n\20\r")
        buf.write("\20\16\20d\3\20\3\20\2\2\21\3\3\5\4\7\5\t\6\13\7\r\b\17")
        buf.write("\t\21\n\23\13\25\f\27\r\31\16\33\17\35\20\37\21\3\2\31")
        buf.write("\4\2DDdd\4\2WWww\4\2KKkk\4\2NNnn\4\2FFff\4\2PPpp\4\2I")
        buf.write("Iii\4\2EEee\4\2QQqq\4\2OOoo\4\2RRrr\4\2GGgg\4\2ZZzz\4")
        buf.write("\2YYyy\4\2VVvv\4\2JJjj\4\2HHhh\4\2TTtt\4\2UUuu\4\2CCc")
        buf.write("c\4\2[[{{\4\2C\\aa\5\2\13\f\17\17\"\"\2l\2\3\3\2\2\2\2")
        buf.write("\5\3\2\2\2\2\7\3\2\2\2\2\t\3\2\2\2\2\13\3\2\2\2\2\r\3")
        buf.write("\2\2\2\2\17\3\2\2\2\2\21\3\2\2\2\2\23\3\2\2\2\2\25\3\2")
        buf.write("\2\2\2\27\3\2\2\2\2\31\3\2\2\2\2\33\3\2\2\2\2\35\3\2\2")
        buf.write("\2\2\37\3\2\2\2\3!\3\2\2\2\5#\3\2\2\2\7%\3\2\2\2\t\'\3")
        buf.write("\2\2\2\13)\3\2\2\2\r+\3\2\2\2\17\64\3\2\2\2\21<\3\2\2")
        buf.write("\2\23A\3\2\2\2\25I\3\2\2\2\27M\3\2\2\2\31T\3\2\2\2\33")
        buf.write("X\3\2\2\2\35]\3\2\2\2\37b\3\2\2\2!\"\7}\2\2\"\4\3\2\2")
        buf.write("\2#$\7\177\2\2$\6\3\2\2\2%&\7.\2\2&\b\3\2\2\2\'(\7]\2")
        buf.write("\2(\n\3\2\2\2)*\7_\2\2*\f\3\2\2\2+,\t\2\2\2,-\t\3\2\2")
        buf.write("-.\t\4\2\2./\t\5\2\2/\60\t\6\2\2\60\61\t\4\2\2\61\62\t")
        buf.write("\7\2\2\62\63\t\b\2\2\63\16\3\2\2\2\64\65\t\t\2\2\65\66")
        buf.write("\t\n\2\2\66\67\t\13\2\2\678\t\f\2\289\t\5\2\29:\t\r\2")
        buf.write("\2:;\t\16\2\2;\20\3\2\2\2<=\t\17\2\2=>\t\4\2\2>?\t\20")
        buf.write("\2\2?@\t\21\2\2@\22\3\2\2\2AB\t\22\2\2BC\t\5\2\2CD\t\n")
        buf.write("\2\2DE\t\n\2\2EG\t\23\2\2FH\t\24\2\2GF\3\2\2\2GH\3\2\2")
        buf.write("\2H\24\3\2\2\2IJ\t\21\2\2JK\t\25\2\2KL\t\24\2\2L\26\3")
        buf.write("\2\2\2MN\t\23\2\2NO\t\n\2\2OP\t\n\2\2PR\t\13\2\2QS\t\24")
        buf.write("\2\2RQ\3\2\2\2RS\3\2\2\2S\30\3\2\2\2TU\t\2\2\2UV\t\26")
        buf.write("\2\2V\32\3\2\2\2WY\t\27\2\2XW\3\2\2\2YZ\3\2\2\2ZX\3\2")
        buf.write("\2\2Z[\3\2\2\2[\34\3\2\2\2\\^\4\62;\2]\\\3\2\2\2^_\3\2")
        buf.write("\2\2_]\3\2\2\2_`\3\2\2\2`\36\3\2\2\2ac\t\30\2\2ba\3\2")
        buf.write("\2\2cd\3\2\2\2db\3\2\2\2de\3\2\2\2ef\3\2\2\2fg\b\20\2")
        buf.write("\2g \3\2\2\2\b\2GRZ_d\3\2\3\2")
        return buf.getvalue()


class buildingLexer(Lexer):

    atn = ATNDeserializer().deserialize(serializedATN())

    decisionsToDFA = [ DFA(ds, i) for i, ds in enumerate(atn.decisionToState) ]

    T__0 = 1
    T__1 = 2
    T__2 = 3
    T__3 = 4
    T__4 = 5
    BUILDING = 6
    COMPLEXB = 7
    WITH = 8
    FLOOR = 9
    HAS = 10
    ROOMS = 11
    BY = 12
    NAME = 13
    NUMBER = 14
    WS = 15

    channelNames = [ u"DEFAULT_TOKEN_CHANNEL", u"HIDDEN" ]

    modeNames = [ "DEFAULT_MODE" ]

    literalNames = [ "<INVALID>",
            "'{'", "'}'", "','", "'['", "']'" ]

    symbolicNames = [ "<INVALID>",
            "BUILDING", "COMPLEXB", "WITH", "FLOOR", "HAS", "ROOMS", "BY", 
            "NAME", "NUMBER", "WS" ]

    ruleNames = [ "T__0", "T__1", "T__2", "T__3", "T__4", "BUILDING", "COMPLEXB", 
                  "WITH", "FLOOR", "HAS", "ROOMS", "BY", "NAME", "NUMBER", 
                  "WS" ]

    grammarFileName = "building.g4"

    def __init__(self, input=None, output:TextIO = sys.stdout):
        super().__init__(input, output)
        self.checkVersion("4.9.2")
        self._interp = LexerATNSimulator(self, self.atn, self.decisionsToDFA, PredictionContextCache())
        self._actions = None
        self._predicates = None


