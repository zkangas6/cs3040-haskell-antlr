grammar building;

@header {
class Floor:
    class Room:
        def __init__(self, x, y):
            self.x = x
            self.y = y

        def area(self):
            return self.x*self.y

    def __init__(self, name):
        self.name = name
        self.rooms = []

    def add_room(self, x, y):
        self.rooms.append(Floor.Room(x,y))

    def area(self):
        x = 0
        for room in self.rooms:
            x += room.area()
        return x


class Building:
    def __init__(self, name):
        self.name = name
        self.floor_list = []

    def add_floor(self, floor):
        if (floor is not None):
            self.floor_list.append(floor)

    def area(self):
        x = 0
        for floor in self.floor_list:
            x += floor.area()
        return x

def print_buildings(list_floors=False):
    total_area = 0
    for building in building_list:
        area = building.area()
        total_area += area
        print("Area for building {name}: {area} square feet.".format(name=building.name, area=area))
        if list_floors:
            print("and has floors:")
            for floor in building.floor_list:
                print("\tfloor", floor.name, "has area", floor.area())
    print("The total usable area is {size} square feet.".format(size=total_area))

floor_list = []
building_list = []
}

plans
    : floor+ complexB EOF
    {print_buildings(list_floors=False)}
    ;

complexB
    : COMPLEXB building+
    ;

building
    : BUILDING name WITH FLOOR '{' floorList '}'
    {
namae= $name.name_val
roomies = $floorList.floor_arr
tfloor = Building(namae)
for i in roomies:
    tfloor.add_floor(i)
building_list.append(tfloor)
}
    ;

floorList returns [floor_arr]
    : floorReference
    {$floor_arr = [$floorReference.floor_x]}
    | floorReference ',' floorList
    {
$floor_arr = $floorList.floor_arr
$floor_arr.append($floorReference.floor_x)}
    ;

floorReference returns [floor_x]
    : floorName
    {namee = $floorName.name_val
$floor_x = None
for fl in floor_list:
    if fl.name == namee:
        $floor_x = fl
fltemp = $floor_x
if fltemp is None:
    raise RecognitionException("Invalid reference to floor: " + namee + "")
}
    ;

floor returns [floor_a]
    : FLOOR name HAS ROOMS '[' roomList ']'
    {namae= $name.name_val
for fl in floor_list:
    if fl.name == namae:
        raise RecognitionException("Duplicate Floor Name: " + namae)
roomies = $roomList.room_arr
tfloor = Floor(namae)
for i in roomies:
    tfloor.add_room(i[0], i[1])
floor_list.append(tfloor)}
    ;

roomList returns [list room_arr]
    : room
    {$room_arr = [$room.room_xy]}
    | room ',' roomList
    {
$room_arr = $roomList.room_arr
$room_arr.append($room.room_xy)
}
    ;

room returns [list room_xy]
    : number room
    {temp = $room.room_xy
temp.append($number.value)
$room_xy=temp}
    | BY number
    {$room_xy = [$number.value]}
    ;

name returns [name_val]
    : NAME
    {$name_val = str($NAME.text)}
    ;

number returns [value]
    : NUMBER
    {$value = int($NUMBER.text)}
    ;

floorName returns [name_val]
    : name
    {$name_val = str($name.name_val)}
    ;


empty
    :
    ;

BUILDING
    : ('b'|'B')('u'|'U')('i'|'I')('L'|'l')('d'|'D')('i'|'I')('n'|'N')('g'|'G')
    ;

COMPLEXB
    : ('c'|'C')('o'|'O')('m'|'M')('p'|'P')('l'|'L')('e'|'E')('x'|'X')
    ;

WITH
    : ('w'|'W')('i'|'I')('t'|'T')('h'|'H')
    ;

FLOOR
    : ('f'|'F')('l'|'L')('o'|'O')('o'|'O')('r'|'R')('s'|'S')?
    ;

HAS
    : ('h'|'H')('a'|'A')('s'|'S')
    ;

ROOMS
    : ('R'|'r')('o'|'O')('o'|'O')('m'|'M')('s'|'S')?
    ;

BY
    : ('b'|'B')('y'|'Y')
    ;

NAME
    : ('A'..'Z'|'_')+
    ;

NUMBER
    : ('0'..'9')+
    ;

WS: [ \n\t\r]+ -> channel(HIDDEN);
