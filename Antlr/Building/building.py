import antlr4
from antlr4.InputStream import InputStream
from antlr4.FileStream import FileStream
from antlr4.error.ErrorStrategy import BailErrorStrategy
from antlr4.error.Errors import ParseCancellationException
from buildingLexer import buildingLexer
from buildingParser import buildingParser
import sys

def main():
    if len(sys.argv) > 1:
        input_stream = FileStream(sys.argv[1])
    else:
        print("Enter your building info")
        print("Input:")
        grammar = ''
        for line in sys.stdin:
            print(line, end="")
            grammar += line
        input_stream = InputStream(grammar)
        print("\r\nOutput:")
    lexer = buildingLexer(input_stream)
    stream = antlr4.CommonTokenStream(lexer)
    parser = buildingParser(stream)
    parser._errHandler = BailErrorStrategy()
    try:
        tree = parser.plans()
        print('SUCCESS')
    except ParseCancellationException as err:
        print("Syntax error in input, discovered by line " + str(lexer.line))

if __name__ == '__main__':
    main()
