grammar adder;

commands
   : (addCommand|aveCommand)+ EOF
   ;

   
aveCommand
   | AVE decimal_list
				{print("Total: " + str($number_list.list_value))}
   ;
   
addCommand
   : ADD number_list
                {print("Total: " + str($number_list.list_value))}
   ;

number_list returns [int list_value]
   : number
                {$list_value = $number.value}
   | number COMMA number_list
                {$list_value = $number.value + $number_list.list_value}
   ;

decimal_list returns [int list_value]
   : decimal
                {$list_value = $decimal.value}
   | decimal COMMA number_list
                {$list_value = $decimal.value + $decimal_list.list_value}
   ;
   
number returns [int value]
   : NUMBER
                {$value = int($NUMBER.text)}
   ;

decimal returns [float value]
   : DECIMAL
				{$value = float($NUMBER.text)}
   ;
   
ADD
   : ('a'|'A')('d'|'D')('d'|'D')
   ;

AVE
   : ('a'|'A')('v'|'V')('e'|'E')
   ;
   
COMMA
   : ','
   ;

NUMBER
   : '-'? ('0'..'9')+
   ;

DECIMAL
   : '-'? ('0'..'9')+ '.'? (('0'..'9')+)?
   ;

WS
   : [ \r\n\t]+ -> channel (HIDDEN)
   ;
