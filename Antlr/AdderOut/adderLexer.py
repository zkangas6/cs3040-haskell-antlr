# Generated from adder.g4 by ANTLR 4.9.2
from antlr4 import *
from io import StringIO
import sys
if sys.version_info[1] > 5:
    from typing import TextIO
else:
    from typing.io import TextIO


#Unused code
def ave(arr):
	length = len(arr)
	total = 0
	for i in arr:
		total += i
	return total/length



def serializedATN():
    with StringIO() as buf:
        buf.write("\3\u608b\ua72a\u8133\ub9ed\u417c\u3be7\u7786\u5964\2\b")
        buf.write("\67\b\1\4\2\t\2\4\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t")
        buf.write("\7\3\2\3\2\3\2\3\2\3\3\3\3\3\3\3\3\3\4\3\4\3\5\5\5\33")
        buf.write("\n\5\3\5\6\5\36\n\5\r\5\16\5\37\3\6\5\6#\n\6\3\6\6\6&")
        buf.write("\n\6\r\6\16\6\'\3\6\3\6\7\6,\n\6\f\6\16\6/\13\6\3\7\6")
        buf.write("\7\62\n\7\r\7\16\7\63\3\7\3\7\2\2\b\3\3\5\4\7\5\t\6\13")
        buf.write("\7\r\b\3\2\7\4\2CCcc\4\2FFff\4\2XXxx\4\2GGgg\5\2\13\f")
        buf.write("\17\17\"\"\2<\2\3\3\2\2\2\2\5\3\2\2\2\2\7\3\2\2\2\2\t")
        buf.write("\3\2\2\2\2\13\3\2\2\2\2\r\3\2\2\2\3\17\3\2\2\2\5\23\3")
        buf.write("\2\2\2\7\27\3\2\2\2\t\32\3\2\2\2\13\"\3\2\2\2\r\61\3\2")
        buf.write("\2\2\17\20\t\2\2\2\20\21\t\3\2\2\21\22\t\3\2\2\22\4\3")
        buf.write("\2\2\2\23\24\t\2\2\2\24\25\t\4\2\2\25\26\t\5\2\2\26\6")
        buf.write("\3\2\2\2\27\30\7.\2\2\30\b\3\2\2\2\31\33\7/\2\2\32\31")
        buf.write("\3\2\2\2\32\33\3\2\2\2\33\35\3\2\2\2\34\36\4\62;\2\35")
        buf.write("\34\3\2\2\2\36\37\3\2\2\2\37\35\3\2\2\2\37 \3\2\2\2 \n")
        buf.write("\3\2\2\2!#\7/\2\2\"!\3\2\2\2\"#\3\2\2\2#%\3\2\2\2$&\4")
        buf.write("\62;\2%$\3\2\2\2&\'\3\2\2\2\'%\3\2\2\2\'(\3\2\2\2()\3")
        buf.write("\2\2\2)-\7\60\2\2*,\4\62;\2+*\3\2\2\2,/\3\2\2\2-+\3\2")
        buf.write("\2\2-.\3\2\2\2.\f\3\2\2\2/-\3\2\2\2\60\62\t\6\2\2\61\60")
        buf.write("\3\2\2\2\62\63\3\2\2\2\63\61\3\2\2\2\63\64\3\2\2\2\64")
        buf.write("\65\3\2\2\2\65\66\b\7\2\2\66\16\3\2\2\2\t\2\32\37\"\'")
        buf.write("-\63\3\2\3\2")
        return buf.getvalue()


class adderLexer(Lexer):

    atn = ATNDeserializer().deserialize(serializedATN())

    decisionsToDFA = [ DFA(ds, i) for i, ds in enumerate(atn.decisionToState) ]

    ADD = 1
    AVE = 2
    COMMA = 3
    NUMBER = 4
    DECIMAL = 5
    WS = 6

    channelNames = [ u"DEFAULT_TOKEN_CHANNEL", u"HIDDEN" ]

    modeNames = [ "DEFAULT_MODE" ]

    literalNames = [ "<INVALID>",
            "','" ]

    symbolicNames = [ "<INVALID>",
            "ADD", "AVE", "COMMA", "NUMBER", "DECIMAL", "WS" ]

    ruleNames = [ "ADD", "AVE", "COMMA", "NUMBER", "DECIMAL", "WS" ]

    grammarFileName = "adder.g4"

    def __init__(self, input=None, output:TextIO = sys.stdout):
        super().__init__(input, output)
        self.checkVersion("4.9.2")
        self._interp = LexerATNSimulator(self, self.atn, self.decisionsToDFA, PredictionContextCache())
        self._actions = None
        self._predicates = None


