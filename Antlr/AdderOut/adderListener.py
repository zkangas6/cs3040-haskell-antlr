# Generated from adder.g4 by ANTLR 4.9.2
from antlr4 import *
if __name__ is not None and "." in __name__:
    from .adderParser import adderParser
else:
    from adderParser import adderParser

#Unused code
def ave(arr):
	length = len(arr)
	total = 0
	for i in arr:
		total += i
	return total/length


# This class defines a complete listener for a parse tree produced by adderParser.
class adderListener(ParseTreeListener):

    # Enter a parse tree produced by adderParser#commands.
    def enterCommands(self, ctx:adderParser.CommandsContext):
        pass

    # Exit a parse tree produced by adderParser#commands.
    def exitCommands(self, ctx:adderParser.CommandsContext):
        pass


    # Enter a parse tree produced by adderParser#aveCommand.
    def enterAveCommand(self, ctx:adderParser.AveCommandContext):
        pass

    # Exit a parse tree produced by adderParser#aveCommand.
    def exitAveCommand(self, ctx:adderParser.AveCommandContext):
        pass


    # Enter a parse tree produced by adderParser#addCommand.
    def enterAddCommand(self, ctx:adderParser.AddCommandContext):
        pass

    # Exit a parse tree produced by adderParser#addCommand.
    def exitAddCommand(self, ctx:adderParser.AddCommandContext):
        pass


    # Enter a parse tree produced by adderParser#number_list.
    def enterNumber_list(self, ctx:adderParser.Number_listContext):
        pass

    # Exit a parse tree produced by adderParser#number_list.
    def exitNumber_list(self, ctx:adderParser.Number_listContext):
        pass


    # Enter a parse tree produced by adderParser#decimal_list.
    def enterDecimal_list(self, ctx:adderParser.Decimal_listContext):
        pass

    # Exit a parse tree produced by adderParser#decimal_list.
    def exitDecimal_list(self, ctx:adderParser.Decimal_listContext):
        pass


    # Enter a parse tree produced by adderParser#number.
    def enterNumber(self, ctx:adderParser.NumberContext):
        pass

    # Exit a parse tree produced by adderParser#number.
    def exitNumber(self, ctx:adderParser.NumberContext):
        pass


    # Enter a parse tree produced by adderParser#decimal.
    def enterDecimal(self, ctx:adderParser.DecimalContext):
        pass

    # Exit a parse tree produced by adderParser#decimal.
    def exitDecimal(self, ctx:adderParser.DecimalContext):
        pass


    # Enter a parse tree produced by adderParser#empty.
    def enterEmpty(self, ctx:adderParser.EmptyContext):
        pass

    # Exit a parse tree produced by adderParser#empty.
    def exitEmpty(self, ctx:adderParser.EmptyContext):
        pass



del adderParser