grammar adder;

@header {
#Unused code
def ave(arr):
	length = len(arr)
	total = 0
	for i in arr:
		total += i
	return total/length
}

commands
   : (addCommand|aveCommand)+ EOF
   ;
   
aveCommand
   : AVE decimal_list
				{print("Average: " + str(($decimal_list.list_value)/$decimal_list.count))}
   ;
   
addCommand
   : ADD number_list
                {print("Added: " + str($number_list.list_value))}
   ;

number_list returns [int list_value]
   : number
                {$list_value = $number.value}
   | number COMMA number_list
                {$list_value = $number.value + $number_list.list_value}
   ;

decimal_list returns [float list_value, int count]
   : decimal
                {$list_value = $decimal.value
$count = 1}
   | decimal COMMA decimal_list
                {$list_value = $decimal.value + $decimal_list.list_value
$count = 1 + $decimal_list.count}
   | empty
{$list_value = 0
$count = 1}
   ;
   
number returns [int value]
   : NUMBER
                {$value = int($NUMBER.text)}
   ;

decimal returns [float value]
   : DECIMAL
				{$value = float($DECIMAL.text)}
   | NUMBER
				{$value = float($NUMBER.text)}
   ;
   
ADD
   : ('a'|'A')('d'|'D')('d'|'D')
   ;

AVE
   : ('a'|'A')('v'|'V')('e'|'E')
   ;
   
empty
   :
   ;
   
COMMA
   : ','
   ;

NUMBER
   : '-'? ('0'..'9')+
   ;

DECIMAL
   : '-'? ('0'..'9')+ '.' ('0'..'9')*
   ;

WS
   : [ \r\n\t]+ -> channel (HIDDEN)
   ;
