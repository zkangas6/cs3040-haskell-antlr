# Generated from adder.g4 by ANTLR 4.9.2
# encoding: utf-8
from antlr4 import *
from io import StringIO
import sys
if sys.version_info[1] > 5:
	from typing import TextIO
else:
	from typing.io import TextIO


#Unused code
def ave(arr):
	length = len(arr)
	total = 0
	for i in arr:
		total += i
	return total/length


def serializedATN():
    with StringIO() as buf:
        buf.write("\3\u608b\ua72a\u8133\ub9ed\u417c\u3be7\u7786\u5964\3\b")
        buf.write("E\4\2\t\2\4\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7\4\b")
        buf.write("\t\b\4\t\t\t\3\2\3\2\6\2\25\n\2\r\2\16\2\26\3\2\3\2\3")
        buf.write("\3\3\3\3\3\3\3\3\4\3\4\3\4\3\4\3\5\3\5\3\5\3\5\3\5\3\5")
        buf.write("\3\5\3\5\5\5+\n\5\3\6\3\6\3\6\3\6\3\6\3\6\3\6\3\6\3\6")
        buf.write("\3\6\3\6\5\68\n\6\3\7\3\7\3\7\3\b\3\b\3\b\3\b\5\bA\n\b")
        buf.write("\3\t\3\t\3\t\2\2\n\2\4\6\b\n\f\16\20\2\2\2B\2\24\3\2\2")
        buf.write("\2\4\32\3\2\2\2\6\36\3\2\2\2\b*\3\2\2\2\n\67\3\2\2\2\f")
        buf.write("9\3\2\2\2\16@\3\2\2\2\20B\3\2\2\2\22\25\5\6\4\2\23\25")
        buf.write("\5\4\3\2\24\22\3\2\2\2\24\23\3\2\2\2\25\26\3\2\2\2\26")
        buf.write("\24\3\2\2\2\26\27\3\2\2\2\27\30\3\2\2\2\30\31\7\2\2\3")
        buf.write("\31\3\3\2\2\2\32\33\7\4\2\2\33\34\5\n\6\2\34\35\b\3\1")
        buf.write("\2\35\5\3\2\2\2\36\37\7\3\2\2\37 \5\b\5\2 !\b\4\1\2!\7")
        buf.write("\3\2\2\2\"#\5\f\7\2#$\b\5\1\2$+\3\2\2\2%&\5\f\7\2&\'\7")
        buf.write("\5\2\2\'(\5\b\5\2()\b\5\1\2)+\3\2\2\2*\"\3\2\2\2*%\3\2")
        buf.write("\2\2+\t\3\2\2\2,-\5\16\b\2-.\b\6\1\2.8\3\2\2\2/\60\5\16")
        buf.write("\b\2\60\61\7\5\2\2\61\62\5\n\6\2\62\63\b\6\1\2\638\3\2")
        buf.write("\2\2\64\65\5\20\t\2\65\66\b\6\1\2\668\3\2\2\2\67,\3\2")
        buf.write("\2\2\67/\3\2\2\2\67\64\3\2\2\28\13\3\2\2\29:\7\6\2\2:")
        buf.write(";\b\7\1\2;\r\3\2\2\2<=\7\7\2\2=A\b\b\1\2>?\7\6\2\2?A\b")
        buf.write("\b\1\2@<\3\2\2\2@>\3\2\2\2A\17\3\2\2\2BC\3\2\2\2C\21\3")
        buf.write("\2\2\2\7\24\26*\67@")
        return buf.getvalue()


class adderParser ( Parser ):

    grammarFileName = "adder.g4"

    atn = ATNDeserializer().deserialize(serializedATN())

    decisionsToDFA = [ DFA(ds, i) for i, ds in enumerate(atn.decisionToState) ]

    sharedContextCache = PredictionContextCache()

    literalNames = [ "<INVALID>", "<INVALID>", "<INVALID>", "','" ]

    symbolicNames = [ "<INVALID>", "ADD", "AVE", "COMMA", "NUMBER", "DECIMAL", 
                      "WS" ]

    RULE_commands = 0
    RULE_aveCommand = 1
    RULE_addCommand = 2
    RULE_number_list = 3
    RULE_decimal_list = 4
    RULE_number = 5
    RULE_decimal = 6
    RULE_empty = 7

    ruleNames =  [ "commands", "aveCommand", "addCommand", "number_list", 
                   "decimal_list", "number", "decimal", "empty" ]

    EOF = Token.EOF
    ADD=1
    AVE=2
    COMMA=3
    NUMBER=4
    DECIMAL=5
    WS=6

    def __init__(self, input:TokenStream, output:TextIO = sys.stdout):
        super().__init__(input, output)
        self.checkVersion("4.9.2")
        self._interp = ParserATNSimulator(self, self.atn, self.decisionsToDFA, self.sharedContextCache)
        self._predicates = None




    class CommandsContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def EOF(self):
            return self.getToken(adderParser.EOF, 0)

        def addCommand(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(adderParser.AddCommandContext)
            else:
                return self.getTypedRuleContext(adderParser.AddCommandContext,i)


        def aveCommand(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(adderParser.AveCommandContext)
            else:
                return self.getTypedRuleContext(adderParser.AveCommandContext,i)


        def getRuleIndex(self):
            return adderParser.RULE_commands

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterCommands" ):
                listener.enterCommands(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitCommands" ):
                listener.exitCommands(self)




    def commands(self):

        localctx = adderParser.CommandsContext(self, self._ctx, self.state)
        self.enterRule(localctx, 0, self.RULE_commands)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 18 
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while True:
                self.state = 18
                self._errHandler.sync(self)
                token = self._input.LA(1)
                if token in [adderParser.ADD]:
                    self.state = 16
                    self.addCommand()
                    pass
                elif token in [adderParser.AVE]:
                    self.state = 17
                    self.aveCommand()
                    pass
                else:
                    raise NoViableAltException(self)

                self.state = 20 
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                if not (_la==adderParser.ADD or _la==adderParser.AVE):
                    break

            self.state = 22
            self.match(adderParser.EOF)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class AveCommandContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser
            self._decimal_list = None # Decimal_listContext

        def AVE(self):
            return self.getToken(adderParser.AVE, 0)

        def decimal_list(self):
            return self.getTypedRuleContext(adderParser.Decimal_listContext,0)


        def getRuleIndex(self):
            return adderParser.RULE_aveCommand

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterAveCommand" ):
                listener.enterAveCommand(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitAveCommand" ):
                listener.exitAveCommand(self)




    def aveCommand(self):

        localctx = adderParser.AveCommandContext(self, self._ctx, self.state)
        self.enterRule(localctx, 2, self.RULE_aveCommand)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 24
            self.match(adderParser.AVE)
            self.state = 25
            localctx._decimal_list = self.decimal_list()
            print("Average: " + str((localctx._decimal_list.list_value)/localctx._decimal_list.count))
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class AddCommandContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser
            self._number_list = None # Number_listContext

        def ADD(self):
            return self.getToken(adderParser.ADD, 0)

        def number_list(self):
            return self.getTypedRuleContext(adderParser.Number_listContext,0)


        def getRuleIndex(self):
            return adderParser.RULE_addCommand

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterAddCommand" ):
                listener.enterAddCommand(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitAddCommand" ):
                listener.exitAddCommand(self)




    def addCommand(self):

        localctx = adderParser.AddCommandContext(self, self._ctx, self.state)
        self.enterRule(localctx, 4, self.RULE_addCommand)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 28
            self.match(adderParser.ADD)
            self.state = 29
            localctx._number_list = self.number_list()
            print("Added: " + str(localctx._number_list.list_value))
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Number_listContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser
            self.list_value = None
            self._number = None # NumberContext
            self._number_list = None # Number_listContext

        def number(self):
            return self.getTypedRuleContext(adderParser.NumberContext,0)


        def COMMA(self):
            return self.getToken(adderParser.COMMA, 0)

        def number_list(self):
            return self.getTypedRuleContext(adderParser.Number_listContext,0)


        def getRuleIndex(self):
            return adderParser.RULE_number_list

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterNumber_list" ):
                listener.enterNumber_list(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitNumber_list" ):
                listener.exitNumber_list(self)




    def number_list(self):

        localctx = adderParser.Number_listContext(self, self._ctx, self.state)
        self.enterRule(localctx, 6, self.RULE_number_list)
        try:
            self.state = 40
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,2,self._ctx)
            if la_ == 1:
                self.enterOuterAlt(localctx, 1)
                self.state = 32
                localctx._number = self.number()
                localctx.list_value = localctx._number.value
                pass

            elif la_ == 2:
                self.enterOuterAlt(localctx, 2)
                self.state = 35
                localctx._number = self.number()
                self.state = 36
                self.match(adderParser.COMMA)
                self.state = 37
                localctx._number_list = self.number_list()
                localctx.list_value = localctx._number.value + localctx._number_list.list_value
                pass


        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Decimal_listContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser
            self.list_value = None
            self.count = None
            self._decimal = None # DecimalContext
            self._decimal_list = None # Decimal_listContext

        def decimal(self):
            return self.getTypedRuleContext(adderParser.DecimalContext,0)


        def COMMA(self):
            return self.getToken(adderParser.COMMA, 0)

        def decimal_list(self):
            return self.getTypedRuleContext(adderParser.Decimal_listContext,0)


        def empty(self):
            return self.getTypedRuleContext(adderParser.EmptyContext,0)


        def getRuleIndex(self):
            return adderParser.RULE_decimal_list

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterDecimal_list" ):
                listener.enterDecimal_list(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitDecimal_list" ):
                listener.exitDecimal_list(self)




    def decimal_list(self):

        localctx = adderParser.Decimal_listContext(self, self._ctx, self.state)
        self.enterRule(localctx, 8, self.RULE_decimal_list)
        try:
            self.state = 53
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,3,self._ctx)
            if la_ == 1:
                self.enterOuterAlt(localctx, 1)
                self.state = 42
                localctx._decimal = self.decimal()
                localctx.list_value = localctx._decimal.value
                localctx.count = 1
                pass

            elif la_ == 2:
                self.enterOuterAlt(localctx, 2)
                self.state = 45
                localctx._decimal = self.decimal()
                self.state = 46
                self.match(adderParser.COMMA)
                self.state = 47
                localctx._decimal_list = self.decimal_list()
                localctx.list_value = localctx._decimal.value + localctx._decimal_list.list_value
                localctx.count = 1 + localctx._decimal_list.count
                pass

            elif la_ == 3:
                self.enterOuterAlt(localctx, 3)
                self.state = 50
                self.empty()
                localctx.list_value = 0
                localctx.count = 1
                pass


        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class NumberContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser
            self.value = None
            self._NUMBER = None # Token

        def NUMBER(self):
            return self.getToken(adderParser.NUMBER, 0)

        def getRuleIndex(self):
            return adderParser.RULE_number

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterNumber" ):
                listener.enterNumber(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitNumber" ):
                listener.exitNumber(self)




    def number(self):

        localctx = adderParser.NumberContext(self, self._ctx, self.state)
        self.enterRule(localctx, 10, self.RULE_number)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 55
            localctx._NUMBER = self.match(adderParser.NUMBER)
            localctx.value = int((None if localctx._NUMBER is None else localctx._NUMBER.text))
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class DecimalContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser
            self.value = None
            self._DECIMAL = None # Token
            self._NUMBER = None # Token

        def DECIMAL(self):
            return self.getToken(adderParser.DECIMAL, 0)

        def NUMBER(self):
            return self.getToken(adderParser.NUMBER, 0)

        def getRuleIndex(self):
            return adderParser.RULE_decimal

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterDecimal" ):
                listener.enterDecimal(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitDecimal" ):
                listener.exitDecimal(self)




    def decimal(self):

        localctx = adderParser.DecimalContext(self, self._ctx, self.state)
        self.enterRule(localctx, 12, self.RULE_decimal)
        try:
            self.state = 62
            self._errHandler.sync(self)
            token = self._input.LA(1)
            if token in [adderParser.DECIMAL]:
                self.enterOuterAlt(localctx, 1)
                self.state = 58
                localctx._DECIMAL = self.match(adderParser.DECIMAL)
                localctx.value = float((None if localctx._DECIMAL is None else localctx._DECIMAL.text))
                pass
            elif token in [adderParser.NUMBER]:
                self.enterOuterAlt(localctx, 2)
                self.state = 60
                localctx._NUMBER = self.match(adderParser.NUMBER)
                localctx.value = float((None if localctx._NUMBER is None else localctx._NUMBER.text))
                pass
            else:
                raise NoViableAltException(self)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class EmptyContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser


        def getRuleIndex(self):
            return adderParser.RULE_empty

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterEmpty" ):
                listener.enterEmpty(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitEmpty" ):
                listener.exitEmpty(self)




    def empty(self):

        localctx = adderParser.EmptyContext(self, self._ctx, self.state)
        self.enterRule(localctx, 14, self.RULE_empty)
        try:
            self.enterOuterAlt(localctx, 1)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx





