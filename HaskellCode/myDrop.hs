-- myDrop.hs: one-function file for illustration
-- (this is a comment line)

myDrop n xs = if n <= 0 || null xs
              then xs
              else myDrop (n - 1) (tail xs)

-- notes:
--   indentation is important: that's what tells Haskell the line is a continuation
