--first_two x = if (null x) then (1,1) else
--    if (not (null (head x)) && not (null (head (tail x))))
--    then (head x, head (tail x))
--    else (1,1)


first_even x = if null x then (-1) else 
    if even (head x) == True
    then head x
    else first_even (tail x)

first_two x = (head x, head (tail x))

--Ack Function

ack m n = if m == 0
	then n+1
	else if n == 0 then
	ack (m-1) 1
	else ack (m-1) (ack m (n-1))


