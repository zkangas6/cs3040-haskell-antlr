--
-- bottom-up-eval.hs: construct bottom up parse with an evaluation operation
--   at each reduce step
--
-- This is a partial implementation; it is an exercise to complete it.
--
--import Debug.Trace

import Data.List -- for find operation

type Grammar = [Rule]
type Rule = (Label, Nonterminal, [GSymbol], Action)
data GSymbol = T Terminal | N Nonterminal
               deriving (Eq, Show)
type Label = String
type Terminal = Char
type Nonterminal = String
type Action = [Int] -> Int
type MatchStack = [(GSymbol, Int)]


bnlGrammar :: Grammar
bnlGrammar = [  -- grammar rewritten without epsilon productions
  ("number", "number", [N "bits"], \x -> (head x)),
  ("integer", "number", [N "bits"], \x -> (head x)),
  ("single", "bits", [N "bit"], \x -> (head x)),
  ("many", "bits", [N "bits", N "bit"], \x -> (head x * 2 + x!!1)), --this
                                                                -- TODO: write the action for this production
                                                                -- pseudocode: multiply the value of the Bits by 2 and add the value of Bit
  ("zero", "bit", [T '0'], \x -> 0),
  ("one", "bit", [T '1'], \x -> 1)
 ]
-- \x -> x!!0 (head)
-- \x -> head x + x!!2
 
-- todo: add expression evaluator
--pos = 0
--value = 0
--binaryadd x = value = value + x**pos

eval :: Grammar -> String -> Int
eval grammar input = steps grammar [] input -- Begin with empty stack

-- equivalent of or but for numbers: return first argument if non-negative,
--    otherwise return the second
por x y | x < 0 = y
por x _ = x

-- steps: apply shift & reduce until reach end of input, returning >= 0 on
--        success and <0 on failure
steps :: Grammar -> MatchStack -> String -> Int

-- Acceptance succeeds (start symbol on stack, all input consumed)
steps grammar [(N nonterminal, val)] [] | nonterminal == start_nonterminal = val
  where
    -- Retrieve start symbol
    ((_, start_nonterminal, _, _):_) = grammar

steps grammar to_match input = por shift_result reduce_result
  where

    -- Shift terminal from input to stack
    shift_result = 
        if null input
        then -1
        else steps grammar ((T (head input), 0) : to_match) (tail input)

    -- Reduce prefix on stack to nonterminal
    reduce_result = 
        if null match_stacks
        then -1
        else first_non_negative
                  (map (\to_match -> steps grammar to_match input)
                       match_stacks)
      where
        -- Retrieve relevant reductions
        match_stacks = [ (N n, (apply_expr e [snd (head (take rhs_len to_match))])) : drop rhs_len to_match
                         | (_, n, rhs, e) <- grammar,
                           let rhs_len = length rhs,
                               (reverse rhs) == map fst (take rhs_len to_match) --this
                       ]

apply_expr e stack_elements = if null stack_elements then 0 else e stack_elements  --this
--apply_expr e stack_elements = trace (show stack_elements) 0

-- return first nonnegative value from a list or -1 if no values are non-negative
first_non_negative :: [Int] -> Int
first_non_negative [] = -1
first_non_negative (x:xs) | x >= 0 = x
first_non_negative (_:xs) = first_non_negative xs

-- have
-- [(n term, 3), (T *, 0), (N factor, 5)]
-- need
-- [N term, T *, N factor]

-- for example: if e is (\x -> sum x) and stack_elements is [(N a, 19), (N b, 6)] (assuming a production
--    Something -> A B), then you would compute the list [19, 6] (the second element from each pair)
--    and apply e to that list (in this case, \x -> sum x), giving the result of 25
-- Note: given fun = \x -> 3 * (x !! 0) + 4 * (x !! 1), then (fun [2, 3]) gives 18
-- apply_action e stack_elements = -- TODO: complete this